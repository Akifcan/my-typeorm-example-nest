import { Controller, Get } from '@nestjs/common';
import { AppService } from './app.service';
import { ConfigService } from './config/config.service';

@Controller()
export class AppController {
  constructor(
    private readonly appService: AppService,
    private configService: ConfigService,
  ) {
    console.log(this.configService.database.host);
  }

  @Get()
  getHello(): string {
    return this.appService.getHello();
  }
}
