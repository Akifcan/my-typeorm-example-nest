import { Module } from '@nestjs/common';
import { PersonService } from './person.service';
import { PersonController } from './person.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Person } from './person.entity';
import { UserSubscriber } from './person.subscriber';
import { User } from './user.entity';
import { PersonRepo } from './person.repository';

@Module({
  imports: [TypeOrmModule.forFeature([Person, PersonRepo])],
  providers: [PersonService, UserSubscriber],
  controllers: [PersonController],
})
export class PersonModule {}
