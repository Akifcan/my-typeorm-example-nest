import {
  Connection,
  EntitySubscriberInterface,
  EventSubscriber,
  InsertEvent,
} from 'typeorm';
import { Person } from './person.entity';

@EventSubscriber()
export class UserSubscriber implements EntitySubscriberInterface<Person> {
  constructor(connection: Connection) {
    connection.subscribers.push(this);
  }

  listenTo() {
    return Person;
  }

  beforeInsert(event: InsertEvent<Person>) {
    console.log(event.metadata);

    event.entity.firstname = 'switch';
    console.log(`BEFORE USER INSERTED: `, event.entity);
  }
}
