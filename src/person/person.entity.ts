import { Entity, Column, PrimaryGeneratedColumn } from 'typeorm';
@Entity({
  name: 'person',
})
export class Person {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  firstname: string;

  @Column()
  lastname: string;
}
