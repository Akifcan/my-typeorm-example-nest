import { Inject, Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Connection, Repository } from 'typeorm';
import { Person } from './person.entity';
import { PersonRepo } from './person.repository';
import { User } from './user.entity';

@Injectable()
export class PersonService {
  @InjectRepository(Person)
  private usersRepository: Repository<Person>;

  @InjectRepository(PersonRepo)
  private readonly personRepository: PersonRepo;

  @Inject() private connection: Connection;

  async findAll(): Promise<Person[]> {
    this.personRepository.test();

    await this.personRepository.like();
    return await this.usersRepository.find();
  }
  async add() {
    return await this.usersRepository.insert({
      id: 42,
      firstname: 'i',
      lastname: 'e',
    });
  }
}
