import { Controller, Get, Inject } from '@nestjs/common';
import { Person } from './person.entity';
import { PersonService } from './person.service';

@Controller('person')
export class PersonController {
  @Inject() private personService: PersonService;

  @Get()
  async getAll(): Promise<Person[]> {
    return await this.personService.findAll();
  }
  @Get('/add')
  async add() {
    return await this.personService.add();
  }
}
