import { Inject } from '@nestjs/common';
import { Connection, EntityRepository, Repository } from 'typeorm';
import { Person } from './person.entity';

@EntityRepository(Person)
export class PersonRepo extends Repository<Person> {
  @Inject() private connection: Connection;

  constructor() {
    super();
  }
  test() {
    console.log('testasfasdfasdf');
  }

  async like() {
    return this.createQueryBuilder('person')
      .where('firstname LIKE :name', { name: 's%' })
      .getMany();
  }
}
